<?php

/**
 * This file is part of a OXID Cookbook project
 *
 * Version:    1.0
 * Author:     Joscha Krug <krug@marmalade.de>
 * Author URI: http://oxid-kochbuch.de
 */

$css = '<style type="text/css">
.groupExp, .confinput{display:none!important;}
</style>';

$sLangName  = "Deutsch";
$aLang = array(
    'charset'                                   => 'utf-8',
    'SHOP_MODULE_GROUP_toxid_config_not_here'   => 'Configuration in Extensions > TOXID Configuration',
    'SHOP_MODULE_noConfigHere'                  => 'You won\'t find the configuration here!' . $css,
);
<?php

use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface;
use OxidEsales\Eshop\Core\ShopVersion;

class toxid_curl_oxconfig extends toxid_curl_oxconfig_parent
{

    protected static function _moduleID()
    {
        return 'toxid_curl';
    }

    public function saveShopConfVar($varType, $varName, $varVal, $shopId = null, $module = '')
    {
        $moduleId = str_replace("module:", "", $module);
        $ShopVersion = str_replace(".", "", oxNew(ShopVersion::class)->getVersion());
        $ShopVersion = substr($ShopVersion, 0, 3);
        if ($moduleId == self::_moduleID() && $ShopVersion >= 620) {
            $variables = [$varName => $varVal];
            $this->saveModuleConfigVariables($moduleId, $variables);
        }
        parent::saveShopConfVar($varType, $varName, $varVal, $shopId, $module);
    }

    private function saveModuleConfigVariables(string $moduleId, array $variables)
    {
        $moduleConfigurationDaoBridge = $this->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
        $moduleConfiguration          = $moduleConfigurationDaoBridge->get($moduleId);
        if (!empty($moduleConfiguration->getModuleSettings())) {
            foreach ($variables as $name => $value) {
                foreach ($moduleConfiguration->getModuleSettings() as $moduleSetting) {
                    if ($moduleSetting->getName() === $name) {
                        if ($moduleSetting->getType() === 'bool') {
                            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                        }
                        $moduleSetting->setValue($value);
                    }
                }
            }
            $moduleConfigurationDaoBridge->save($moduleConfiguration);
        }
    }
}
